<%@page import="rocksta.user.User"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application | Update User</title>		
        <link href = "css/bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js/jquery-2.1.3.min.js"></script>
        <script src = "js/jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <div class = "container text-center" style = "height:30px">
                <h3>Administration Portal</h3>
                <p>
                    <small>User information Update</small>
                </p>
            </div>
        </div><!--End of jumbotron-->
        <br><br>
        <div class = "container">
            <form action = "management.do.system" method ="POST">
                <table class="table-striped">
                    <%
                        User user = (User)request.getAttribute("user");
                     %>
                    <tr><th>Name</th><th><input type ="text" class="form-control" name ="name" value = <%=user.getName()%>></th> </tr>
                    <tr><th>ID Number</th><th><input type ="text" class="form-control" name ="id" value = <%=user.getId()%> readonly></th> </tr>
                     <tr><th>Surname</th><th><input type ="text" class="form-control" name ="surname" value = <%= user.getSurname() %>></th> </tr>
                     <tr><th>Gender</th><th> <input readonly type ="text" class="form-control" name ="gender" value = <%= user.getAccountNumber() %>> </th> </tr>
                     <tr><th>Account Number</th><th>  <input value = "<%=user.getGender() %>" readonly="" class="form-control" name ="accnum"></th> </tr>
                     <tr><th>Email Address</th> <th><input type ="text" class="form-control" name ="email" value = <%=user.getEmail() %>></th></tr>               
                    <tr><th>Username</th> <th><input type ="text" class="form-control" name ="username" value = <%=user.getUsername() %>></th>
                    </tr><tr><th>Password</th><th><input type ="password" class="form-control" name ="password" value = <%=user.getPassword() %>></th></tr>
                    <tr> <th>Role</th><th>
                            <select name ="role" class="form-control">
                                <option value="<%=user.getRole() %>"><%=user.getRole() %></option>
                                <option value="management">Management</option>
                                <option value="member">Member</option>
                                <option value="admin">Administrator</option>
                            </select></table>
                <input type ="submit" class=" form-control btn btn-danger" name ="selection" value = "Update">
            </form>
        </div>
        <div class = "jumbotron">
                     <div class = "container text-center" style = "height:10px">
                     <small>Rocksta Creations Copy right</small>		
               </div>
        </div><!--End of jumbotron-->

        <div class = "container">	
            <div id = "main_content">
        </div>                                                
    </div>
  <script src = "js\jquery-1.11.1.min.js"></script>
  <script src = "js\bootstrap.min.js"></script>
                                        
  </body>

  </html>