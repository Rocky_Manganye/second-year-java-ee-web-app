<%@page import="java.util.Vector"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="rocksta.donation.Donation"%>
<%@page import="rocksta.user.User"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css/bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js/jquery-2.1.3.min.js"></script>
        <script src = "js/jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
    </head>
    <body>
        <%
            User user = (User) request.getAttribute("user");
        %>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand  btn btn-danger" >Logout</a>
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <form method="POST" action="print.do.system">
                <input name="account" value="<%= user.getAccountNumber()%>" readonly="" class="form-control" type="hidden">
                 <input type ="hidden" class="form-control" name ="uname" value = <%=user.getUsername()%>>
                   <input type ="hidden" class="form-control" name ="psw" value = <%=user.getPassword()%>>
            
                <input name="select" value="Print Report" type="submit" class="btn btn-danger">
            </form>

            <div class = "container text-center" style = "height:30px">
                <h3>Welcome to LightHouse Chapel International : User Portal</h3>
                <p><strong><%= user.getName()%></strong></p>
            </div>

        </div><!--End of jumbotron-->	
        <div class="row">
            <div class="col-lg-4">
                <%
                    ArrayList<Donation> list = (ArrayList<Donation>) request.getAttribute("donations");
                    java.util.Date date = new java.util.Date();
                %>
                <h1>Tithe Record</h1>
                <table  class = "table table-responsive">
                    <thead>
                        <tr>
                            <th> Tithe Amount</th>
                            <th>Tithe Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (int x = 0; x < list.size(); ++x) {
                        %>
                        <tr>
                            <th><%= list.get(x).getAmount()%> </th>
                            <th><%= list.get(x).getDate()%></th>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-3">
                <form action="management.do.system" method="POST">
                    <label class="form-control">Account Number </label>
                    <input name="account" value="<%= user.getAccountNumber()%>" readonly="" class="form-control">
                    <label class="form-control">Enter Amount to Tithe (R)</label>
                    <input type="number" class="form-control" name="amount" required placeholder="Enter Amount to Tithe" maxlength=""> 

                    <label class="form-control">Current Date And Time</label>
                    <input name="date" type="text" value="<%= date.toString()%>" class="form-control" readonly="">
                   <input type ="hidden" class="form-control" name ="uname" value = <%=user.getUsername()%>>
                   <input type ="hidden" class="form-control" name ="psw" value = <%=user.getPassword()%>>
            <tr> 
                    <input name="selection" value="Tithe" type="submit" class="btn btn-primary">
                </form>
            </div>
            <div class="col-lg-4">
                <form action="" method="POST">
                    <label class="form-control">Account Number</label>
                    <input type="text" value="<%= user.getAccountNumber()%>" readonly="" class="form-control" name="account">                    
                    <label class="form-control">Project Name</label>
                    <select class="form-control" name="Project" required="">
                        <%
                            ArrayList<String> projects = (ArrayList<String>) request.getAttribute("projects");
                            for (int x = 0; x < projects.size(); ++x) {
                                String[] data = projects.get(x).split("@");
                        %>
                        <option value="<%= data[1]%>"><%= data[1]%></option>
                        <%
                            }
                            request.setAttribute("user", user);
                        %>
                    </select>
                    <label class="form-control">Amount</label>
                    <input type="number"  class="form-control" name="amount" required="">  

                    <label class="form-control">Description</label>
                    <input type="text"  class="form-control" name="description" required="">     
                    <input type ="hidden" class="form-control" name ="uname" value = <%=user.getUsername()%>>
                   <input type ="hidden" class="form-control" name ="psw" value = <%=user.getPassword()%>>
            
                    <input name="selection" value="Donate to project" type="submit" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div>
            <hr>
            <div class="col-lg-5">
                <strong>PROFILE</strong>
        <table class="table-condensed">
            <tr><th>Name</th><th><input readonly="" type ="text" class="form-control alert-danger" name ="name" value = <%=user.getName()%> ></th>
                <th>ID Number</th><th><input type ="text" readonly class="form-control alert-danger" name ="id" value = <%=user.getId()%> ></th> </tr>
            <tr><th>Surname</th><th><input readonly type ="text" class="form-control alert-danger" name ="surname" value = <%= user.getSurname()%>></th>
                <th>Gender</th><th> <input readonly type ="text" class="form-control  alert-danger" name ="gender" value = <%=user.getGender()%>> </th> </tr>
            <tr><th>Account Number</th><th>  <input value = "<%= user.getAccountNumber()%>" readonly="" class="form-control alert-danger" name ="accnum"></th> 
                <th>Email Address</th> <th><input type ="text" class="form-control alert-danger" name ="email" value = <%=user.getEmail()%>></th></tr>               
            <tr><th>Username</th> <th><input type ="text" class="form-control" name ="uname" value = <%=user.getUsername()%>></th>
                
                <th>Password</th><th><input type ="password" class="form-control" name ="psw" value = <%=user.getPassword()%>></th></tr>
            <tr> <th>Role</th><th>

                    <input value="<%=user.getRole()%>" class="form-control alert-danger" type="text" readonly="">
        </table>
            </div>
            <div></div>
            <div class="col-lg-5">
                <strong>MY DONATION RECORD</strong>
                <table class="table table- table-bordered">                    
                    <tr class="alert-danger">
                         <td>Donation Number</td>
                         <td>Account Number</td>
                         <td>Donated Amount</td>
                     </tr>
                <%
                    request.setAttribute("logged",user);
                    Vector<String> data1 = (Vector<String>)request.getAttribute("dons");
                    if(!data1.isEmpty())
                    {
                        for(int x = 0;x < data1.size();++x)
                        {
                            String elements [] = data1.get(x).split("@");
                            if(elements[1].equalsIgnoreCase(user.getAccountNumber())){
                %>
                            <tr>
                                 <td><%=elements[0]%></td>
                                 <td><%=elements[1]%></td>
                                 <td><%=elements[2]%></td>
                             </tr>
                <%
                        }}
                    }
                %>
                
                </table>
            </div>
        </div>
        
        <div id="side" >
            <table class ="table" >

            </table>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class = "jumbotron">
    <div class = "container text-center" style = "height:10px">
        <small>Rocksta Creations Copy right</small>		
    </div>
</div><!--End of jumbotron-->

<div class = "container">	
    <div id = "main_content">
    </div>                                                
</div>
<script src = "js\jquery-1.11.1.min.js"></script>
<script src = "js\bootstrap.min.js"></script>

</body>

</html>