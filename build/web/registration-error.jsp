<%-- 
    Document   : registration_error
    Created on : Apr 14, 2015, 7:13:22 AM
    Author     : ROCKY MANGANYE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
        <link href = "css\bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js\jquery-2.1.3.min.js"></script>
        <script src = "js\jquery.validate.js"></script>
        <script src = "js\jquery-1.11.1.min.js"></script>
       <script src = "js\bootstrap.min.js"></script>                                                                                       
    </head>
    <body>
        <h1>Please ensure that these conditions are met before submitting the registration Form</h1>
        <div class="h2 alert-danger">
            Conditions
            <ul >
                <li>Name and Surname should have a minimum of 2 characters each</li>
                <li>ID Number should have 13 characters</li>
                <li>Username should have a minimum of 5 characters</li>
                <li>Password should have a minimum of 8 characters</li>
            </ul>  
            <a href="registration.jsp" class="btn btn-primary btn-lg">Click Me</a>
        </div>
    </body>
</html>
