<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css\bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js\jquery-2.1.3.min.js"></script>
        <script src = "js\jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand" >Home</a>
                    <a href ="register.html" class = "navbar-brand active" >Register</a>
                    <a href ="contact-us.jsp" class = "navbar-brand" >Contact Us</a>
                    <a href ="about-us.jsp" class = "navbar-brand" >About Us</a>
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <div class = "container text-center" style = "height:30px">
                <h3>Registration form</h3>
                <p>
                    <small>Please fill in all the fields</small>
                </p>
            </div>
        </div><!--End of jumbotron-->
        <br><br>
        <div class = "container">	
            <div id = "main_content">
                <form action = "management.do.system" method = "POST">				
                    <label for = "name" >Name<input type = "text" name = "user_name" class = "form-control " placeholder = "enter your name" id = "user_name" required autofocus>
                        <label for = "surname" >Surname<input type = "text" name = "usersurname" class = "form-control " placeholder = "enter your surname" id = "usersurname" required>
                            <label for = "id" >ID number<input type = "text" name = "userid" class = "form-control required" placeholder = "enter your id number" id = "userid" required>
                                        <label for="inputEmail">Email address</label>
                                        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name = "useremail" required >
                                        <label for = "username">Username</label>
                                        <input type = "text" class = "form-control required" id = "username" placeholder = "enter your username" size = "20" required name = "username">
                                        <label for = "password">Password</label>
                                        <input type ="password" class ="form-control required" id = "password" placeholder="enter your password" size = "20" required name = "password">
                                        <br>
                                        <input type = "submit" class = "btn btn-danger" name = "selection" value = "Register">					
                                        </form>
                                        </div>
                                        <div class="h2">
                                             Conditions
                                            <ul >
                                                <li>Name and Surname should have a minimum of 2 characters each</li>
                                                <li>ID Number should have 13 characters</li>
                                                <li>Username should have a minimum of 5 characters</li>
                                                <li>Password should have a minimum of 8 characters</li>
                                            </ul>
                                            
                                        </div>
         
                                        </div>
                                        <div class = "jumbotron">
                                            <div class = "container text-center" style = "height:10px">
                                                <small>Rocksta Creations Copy right</small>		
                                            </div>
                                        </div><!--End of jumbotron-->

                                        <div class = "container">	
                                            <div id = "main_content">
                                            </div>
                                                
                                        </div>
                                                <script src = "js\jquery-1.11.1.min.js"></script>
                                                <script src = "js\bootstrap.min.js"></script>
                                                <script>
                                                    $("document").ready(function () {
                                                        $("form").validate();
                                                    })
                                                </script>
                                                </body>

  </html>