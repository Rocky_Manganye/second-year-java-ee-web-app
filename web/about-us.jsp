<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css/bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js/jquery-2.1.3.min.js"></script>
        <script src = "js/jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand" >Home</a>
                    <a href ="registration.jsp" class = "navbar-brand" >Register</a>
                    <a href ="contact-us.jsp" class = "navbar-brand" >Contact Us</a>
                    <a href ="about-us.jsp" class = "navbar-brand active" >About Us</a>
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <div class = "container text-center" style = "height:30px">
                 <h3>Welcome to LightHouse Chapel International</h3>
            </div>
        </div><!--End of jumbotron-->
        
        <div class = "row">	
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <strong>STATEMENT OF FAITH</strong><br><br>
                <br>
                <strong>OUR BELIEFS</strong><br>
                <ul>
                    <li>That God Almighty is the one and only true God</li>
                    <li>That He is three-in-one, Father,Son and Holy Spirit, and Creator of all nature</li>
                    <li>That Jesus Christ , born of the virgin Mary is the only begotten son of the Father</li>
                    <li>That He is God manifested and dwelt among men</li>
                </ul>
                <br>
                <br>
                <strong>OUR PURPOSE</strong><br>
                <ul>
                    <li>To provide a solid foundation of Bible-based instruction,equipping our members to preach and teach the Gospel,While abiding by the laws of the country where the church is located</li>
                </ul>
                
                 <strong>OUR Mission</strong><br>
                <ul>
                    <li>To build 25,000 churches</li>
                    <li>To have churches in 150 countries</li>
                    <li>To fight fiercely and relentlessly in all battles for the advancement of the churches of the Gospel</li>
                    <li>To produce radical Christians who work for God</li>
                    <li>To go to heaven and to hear Jesus say - "Well done,good and faithful servant"</li>
                </ul>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class = "jumbotron">
                     <div class = "container text-center" style = "height:10px">
                     <small>Rocksta Creations Copy right</small>		
               </div>
        </div><!--End of jumbotron-->

        <div class = "container">	
            <div id = "main_content">
        </div>                                                
    </div>
  <script src = "js\jquery-1.11.1.min.js"></script>
  <script src = "js\bootstrap.min.js"></script>
                                        
  </body>

  </html>