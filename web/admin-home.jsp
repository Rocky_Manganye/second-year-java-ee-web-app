<%@page import="rocksta.user.User"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css/bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js/jquery-2.1.3.min.js"></script>
        <script src = "js/jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}	
            #wrapper{padding:25px 0 135px 0;overflow:hidden;width:941px;border-top:1px solid #fff;
                     font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#333333;}
            html{
                width:100%;
                height:100%;
            }
            body{
                width:941px;
                height:100%;
                position:relative;
                margin:0 auto;
            }
            html > body{
                height:auto;
                min-height:100%;
            }
            .block{
                padding:20px 0 5px 0;
                overflow:hidden;
            }
            .block img{
                float:left;
                margin:3px 25px 0 0;
            }
            .block p{
                display:block;
                float:left;
                width:236px;
            }
            .news{
                padding:0 21px 0 19px;
                float:left;
            }
        </style>
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand btn btn-default" >Logout</a>                    
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">

            <div class = "container text-center" style = "height:30px">
                <h3>Administration Portal</h3>  
                <a href ="admin-reg-management.jsp" class ="btn btn-info">Register Management</a>
            </div>
        </div><!--End of jumbotron-->      

        <form action="management.do.system" method="POST">
            <table class ="table table-bordered table-responsive">
                <thead class = "text-danger text-justify">
                    <tr class ="alert-success">
                        <td>Select User</td>
                        <td>Name</td>
                        <td>Surname</td>
                        <td>Gender</td>                        
                        <td>Username</td>
                        <td>Email Address</td>
                        <td>Account Number</td>
                    </tr>
                </thead>
                <tbody>
                    <%
                        java.util.ArrayList<User> users = (java.util.ArrayList<User>) request.getAttribute("users");
                        for (User user : users) {
                    %>
                    <tr>
                        <td>
                            <input type = "radio" class="radio-inline" name ="userid" value ="<%= user.getId()%>" >
                        </td>
                        <td>
                            <%= user.getName()%>
                        </td>
                        <td>
                            <%= user.getSurname()%>
                        </td>
                        <td>
                            <%= user.getGender()%>
                        </td>

                        <td>
                            <%= user.getUsername()%>
                        </td>
                        <td>
                            <%= user.getEmail()%>
                        </td>
                        <td>
                            <%= user.getAccountNumber()%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
                <tfoot>
                    <tr class = "well alert-warning">
                        <td>
                            Total Number of Users in the system : <%= users.size()%>
                        </td>
                    </tr>

                </tfoot>
            </table>
            <div class ="btn-group-lg ">
                <input type = "submit" class="btn btn-primary" name="selection" value="Update User Information">
                <input type = "submit" class="btn btn-danger" name="selection" value="Delete">
            </div>
        </form>
        <br><br><br>
        <form method="POST" action="print.do.system">
            <input name="select" value="Print User Information" type="submit" class="btn btn-danger btn-lg">
        </form>
        <div class = "jumbotron">
            <div class = "container text-center" style = "height:10px">
                <small>Rocksta Creations Copy right</small>		
            </div>
        </div><!--End of jumbotron-->

        <script src = "js\jquery-1.11.1.min.js"></script>
        <script src = "js\bootstrap.min.js"></script>

    </body>

</html>