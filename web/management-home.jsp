<%-- 
    Document   : management-home
    Created on : Mar 26, 2015, 2:57:29 AM
    Author     : ROCKY MANGANYE
--%>

<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css\bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css\bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js\jquery-2.1.3.min.js"></script>
        <script src = "js\jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
        
       
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand btn btn-warning">Logout</a>                    
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <div class="row">
                <div class="col-lg-2"><img src='images/logo.jpg' height="100"></div>               
                <div class="col-lg-8">
                    <div class = "container text-center" style = "height:30px">
                 
                <h3> LightHouse Chapel International : Management Portal</h3>
                
                
            </div>
                </div>
                 <form action="printall.do">
            <input name="select" value="Print Information" type="submit" class="btn btn-danger btn-lg">
        </form>
            </div>
            
        </div><!--End of jumbotron-->
        <br><br>
        <div class="row">
            <div class="col-lg-3">
                <h1>Tithe Record of All Users</h1>
                      <table  class = "table table-responsive">
                          <thead>
                              <tr class="alert-warning">
                                <th>
                                    Tithe Amount
                                </th>
                                 <th>
                                   Tithe Date
                                </th>
                              </tr>
                          </thead>
                          <%
                            
                              double total = 0.00;
                              java.util.ArrayList<rocksta.donation.Donation> list = (java.util.ArrayList<rocksta.donation.Donation>)request.getAttribute("donations");
                              if(!list.isEmpty()){
                                 
                              for(int x = 0;x < list.size();++x)
                              {
                                  total += list.get(x).getAmount();
                          %>
                                <tbody>
                                    <tr>
                                      <th>
                                          <%= list.get(x).getAmount() %>
                                      </th>
                                       <th>
                                          <%= list.get(x).getDate() %>
                                      </th>
                                    </tr>
                                
                          <%
                              }}
                          %>   
                          
                          <tr>
                              <th>Total User Tithe : <input class="form-control alert-success" type ="text" readonly="" value = "R <%= total %>"></th>
                          </tr>
                         
                        </tbody>
                      </table>
            </div>
            <div class="col-lg-5">
                <table class=" table table-responsive">
                    <tr class="alert-info">
                        <td>Project Number</td>
                        <td>Project Name</td>
                        <td>Start Date</td>
                        <td>End Date</td>
                        <td>Cost of Project</td>
                    </tr>
                <%
                    ArrayList<String> projects = (ArrayList<String>)request.getAttribute("projects");
                    if(!projects.isEmpty()){
                    for(int x = 0;x < projects.size();++x){
                        
                    String [] data = projects.get(x).split("@");
                %>    
                    <tr>
                        <td><%= data[0] %></td>
                        <td><%= data[1] %></td>
                        <td><%= data[2] %></td>
                        <td><%= data[3] %></td>
                        <td><%= data[4] %></td>
                    </tr>
                <%
                    }}
                %>
                </table>
                <br>
                <hr>
                <table class="table table- table-bordered">                    
                    <tr class="alert-danger">
                         <td>Donation Number</td>
                         <td>Account Number</td>
                         <td>Donated Amount</td>
                     </tr>
                <%
                    Vector<String> data1 = (Vector<String>)request.getAttribute("dons");
                    if(!data1.isEmpty())
                    {
                        for(int x = 0;x < data1.size();++x)
                        {
                            String elements [] = data1.get(x).split("@");
                %>
                            <tr>
                                 <td><%=elements[0]%></td>
                                 <td><%=elements[1]%></td>
                                 <td><%=elements[2]%></td>
                             </tr>
                <%
                        }
                    }
                %>
                
                </table>
            </div>
             <div class="col-lg-4">
                 <span class="well">New Project</span>
                 <br><br>
                 <form class="form_settings" method="POST" action="management.do.system">
                     <input type="text" class="form-control" name="proj_name" placeholder="Enter the project name" id='proj_name' required="">
                     <select name="start_day" id='proj_start_day' class="form-control">
                         <option>Project Start Day</option>
                            <%
                               for(int x = 1 ; x < 32;x++)
                               {
                            %>
                                   <option value="<%= x %>"><%= x %></option>
                            <%
                               } 
                            %>
                     </select>
                     
                     <select name="start_month" id="proj_start_month" class="form-control" required="">
                        <option>Project Start Month</option>                            
                         <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                     </select>
                     
                     <select name="start_year" id="proj_start_year" class="form-control" required="">
                        <option>Project Start Year</option>
                        <option value="2015">2015</option>   
                        <option value="2015">2016</option>   
                        <option value="2015">2017</option>   
                     </select>
                     
                     <br>
                    
                      <select name="end_day" id='proj_end_day' class="form-control" required="">
                         <option>Project End Day</option>
                            <%
                               for(int x = 1 ; x < 32;x++)
                               {
                            %>
                                   <option value="<%= x %>"><%= x %></option>
                            <%
                               } 
                            %>
                     </select>
                     
                     <select name="end_month" id="proj_end_month" class="form-control" required="">
                        <option>Project End Month</option>                            
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                     </select>
                     
                     <select name="end_year" id="proj_end_year" class="form-control" required="">
                        <option>Project End Year</option>
                        <option value="2015">2015</option>   
                        <option value="2015">2016</option>   
                        <option value="2015">2017</option>   
                     </select>
                     <br>
                     <input name="proj_cost" type="text" class="form-control" placeholder="Enter the cost of the project" required="">
                     <br>
                     
                     <input type="submit" name="selection" class="btn btn-danger" value="Create New Project">                     
                 </form>                 
             </div>
        </div>
      
        <div class = "jumbotron">
                     <div class = "container text-center" style = "height:10px">
                     <small>Rocksta Creations Copy right</small>		
               </div>
        </div><!--End of jumbotron-->

        <div class = "container">	
            <div id = "main_content">
        </div>                                                
    </div>
  <script src = "js\jquery-1.11.1.min.js"></script>
  <script src = "js\bootstrap.min.js"></script>
                                        
  </body>

  </html>