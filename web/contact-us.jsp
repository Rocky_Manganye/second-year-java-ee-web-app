<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Church Application</title>		
        <link href = "css/bootstrap.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap.min.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.css" rel = "stylesheet" type = "text/css">
        <link href = "css/bootstrap-theme.min.css" rel = "stylesheet" type = "text/css">
        <script src = "js/jquery-2.1.3.min.js"></script>
        <script src = "js/jquery.validate.js"></script>
        <style>
            #main_content{float:left;margin-left : 250px;padding:10px;width:300px;}
            #side{float:left;padding:10px;width:602px;}		
        </style>
    </head>
    <body>
        <nav class = "navbar navbar-inverse navbar-fixed-top" id = "my-navbar" ><!--style= "padding-left:350px"-->
            <div class = "container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle" data-toggle ="collpase" data-target = "#navbar-collapse">
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                        <span class = "icon-bar"></span>						
                    </button>
                    <a href ="index.html" class = "navbar-brand" >Home</a>
                    <a href ="registration.jsp" class = "navbar-brand" >Register</a>
                    <a href ="contact-us.jsp" class = "navbar-brand active" >Contact Us</a>
                    <a href ="about-us.jsp" class = "navbar-brand" >About Us</a>
                </div><!-- End of navbar header-->

            </div><!--End of Container -->
        </nav><!--End of nav bar -->
        <div class = "jumbotron">
            <div class = "container text-center" style = "height:30px">
                <h3>Contact Us : LightHouse Chapel International</h3>
            </div>
        </div><!--End of jumbotron-->
        <br><br>
        <div class = "row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <strong>ADDRESS</strong>
                <br>
                <address>
                    258 Kotze Street<br>
                    Sunnyside<br>
                    Pretoria<br>
                    0002<br>
                    Republic of South Africa
                </address>
                
                <br>
                <br>
                <strong>CALL</strong>
                <br>
                <address>
                    +27 12 343 8895<br>
                    +27 12 343 8810<br>
                </address>
                
                <br>
                <br>
                
                <strong>SERVICE TIMES</strong>
                <br>
                <address>
                    <strong>Sunday : </strong> 07am,10am,12pm<br>
                    <strong>Evening Service : </strong> 06pm<br>
                    <strong>Tuesday : </strong> 06pm<br>
                </address>
              </div>
            <div class="col-lg-5">
                <img src="images/map.png">
              </div>
        </div>
        <div class = "jumbotron">
                     <div class = "container text-center" style = "height:10px">
                     <small>Rocksta Creations Copy right</small>		
               </div>
        </div><!--End of jumbotron-->

        <div class = "container">	
            <div id = "main_content">
        </div>                                                
    </div>
  <script src = "js\jquery-1.11.1.min.js"></script>
  <script src = "js\bootstrap.min.js"></script>
                                        
  </body>

  </html>